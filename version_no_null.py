# -*- coding: utf-8 -*-
# Modulprojekt CLT
# Authorin: Sandra Sánchez
# Datum: 21.02.2022

#First: download corpus
#nltk.download('punkt')

import sys
import spacy
import itertools
import pandas as pd
import numpy as np
import nltk
import re
import time
from nltk import word_tokenize
#nltk.download('punkt')
from itertools import chain
from tqdm import tqdm
from copy import deepcopy
import operator


start = time.time()

# CLASS PREPROCESS

def read_corpus(corpus_file):
    with open(corpus_file, encoding='utf-8') as file:
        text = file.read()  # Text as string
    text = text.strip().split('\n')  # Split into sentences
    return text

def strip_non_alfanum(word):
    return re.sub(r"^\W+|\W+$", "", word)

def tokenise_clean_sents(preprocessed_corpus):
    sents = [s.split() for s in preprocessed_corpus]
    return sents

def tokenize(sentence, append_null=False):
    punctuation = ['!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-',
                   '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^',
                   '_', '`', '{', '|', '}', '~', '``',
                   "''", '--', '¿', '¡', '...', '’', '“', '”', '•',
                   '‰', '€', '≤']  # This list includes more than the string.punctuation() one
    tokens = word_tokenize(sentence)
    #print(tokens)
    tokens = [token.lower() for token in tokens if token not in punctuation]
    clean_tokens = [strip_non_alfanum(token) for token in tokens if token not in punctuation]
    if append_null:
        clean_tokens.append("NULL")  # Only append to Zielsprache sent
    # Need to improve to clean expressions like '–véanse'
    #print(clean_tokens)
    return clean_tokens

def clean_corpus(corpus, append_null=False):
    if append_null:
        clean_corpus = [tokenize(sentence, append_null=True) for sentence in corpus]
        return clean_corpus
    clean_corpus = [tokenize(sentence) for sentence in corpus]
    return clean_corpus

def load_vocab(filename):
    with open(filename, encoding='utf-8') as file:
        text = file.read()  # Text as string
        words = text.split(',')
    return words

# open f_src, f_tgt: # they are preprocessed
#   # first line of f_src and f_tgt (both have the same no. of lines)
#   current_sent_src = f_src.readline()
#   current_sent_tgt = f_tgt.readline()


def create_vocab(tokens):  # Most obvious method, returns list
    vocabulary = sorted(list(set(tokens)))
    return vocabulary

def get_vocab(sentences):  # Most ellegant method, returns set
    words = set(itertools.chain.from_iterable(sentences))
    return words

def get_words(sentences):  # Best way to create vocab
    vocab = set().union(*sentences)
    vocab = sorted(list(vocab))
    return vocab

def initialise(english_set, foreign_set):  # Seems slighlty quicker with a small set, should be more efficient in the long run
    #A
    probs = {}
    initial_prob = 1 / len(foreign_set)  # In the beginning all probabilities are identical
    for f_w in foreign_set:
        for e_w in english_set:
            pair = (e_w, f_w)
            probs[pair] = initial_prob
    #print(probs)
    return probs

def initialise_dict_comp(english_set, foreign_set):  # Seems a bit slower but is more efficient with large sets
    #B
    initial_prob = 1 / len(foreign_set)  # In the beginning all probabilities are identical
    probs = {(e_w, f_w): initial_prob
                         for e_w in english_set
                         for f_w in foreign_set}
    return probs

def initialise_better(english_set, foreign_set):
    # for f_w in foreign_set:
    #     self.translation_table[f_w] = defaultdict(lambda: initial_prob)

    # Initializing t(e|f) uniformly
    d = np.ones(shape=(len(foreign_set), len(english_set)))
    t = pd.DataFrame(d, columns=english_set, index=foreign_set)
    for i in range(len(foreign_set)):
        for j in range(len(english_set)):
            t.iloc[i][j] = t.iloc[i][j] / len(english_set)
    return t

def save_corpus(corpus, filename):
    sentences = [' '.join(item) for item in corpus]
    separated_sentences = [sentence + '\n' for sentence in sentences]
    with open(filename, "w") as file:
        for sentence in separated_sentences:
            file.write(sentence)  # file.write("Hello \n")
    return file

def save_vocab(wordset, filename):
    with open(filename, "w") as file:
        words_string = ",".join(wordset)
        file.write(words_string)
    return file


print("____________TEST______________________")
test_pairs = {'If we consider the Copenhagen criteria, the ultimate benchmark for EU membership, the Albanian Government should be alerted to a number of things.': 'Si consideramos los criterios de Copenhague, que constituyen la referencia comparativa definitiva para la adhesión a la Unión Europea, habría que advertir al Gobierno albanés de varias cosas.', 'I have already mentioned the local elections, which, we assume, everyone will keep free and fair.': 'Ya he mencionado las elecciones locales que, suponemos, entre todos harán que sean libres y justas.', 'We will ensure that they are, while also hoping that the Commission and the Council will keep an eye on things.': 'Garantizaremos que lo sean, confiando también en que la Comisión y el Consejo velen por ello.'}
e_sents = test_pairs.keys()  # One list of string-sentences
f_sents = test_pairs.values()
#print(e_sents)
#print(f_sents)
#The algorithm took 0.04753994941711426 seconds.
#__________________________
# english_sentences = clean_corpus(e_sents)
# print(english_sentences)
# save_corpus(english_sentences, 'clean_corpus_e.txt')
# foreign_sentences = clean_corpus(f_sents, append_null=True)
# save_corpus(foreign_sentences, 'clean_corpus_f.txt')
# voc_e = get_words(english_sentences)
# voc_f = get_words(foreign_sentences)
# # print(voc_e)
# # print(voc_f)
# save_vocab(voc_e, 'e_vocab.txt')
# save_vocab(voc_f, 'f_vocab.txt')
# print('Phase 1 concluded')
# print('___________________________________________')
# prep_e_corpus = read_corpus('clean_corpus_e.txt')
# e_sents = tokenise_clean_sents(prep_e_corpus)
# #print(prep_e_corpus)
# print(e_sents)
# prep_f_corpus = read_corpus('clean_corpus_f.txt')
# f_sents = tokenise_clean_sents(prep_f_corpus)
# print(f_sents)
# print(len(e_sents))
# print(len(f_sents))
# #print(prep_e_corpus)
# #print(prep_f_corpus)
# voc_e = load_vocab('e_vocab.txt')
# voc_f = load_vocab('f_vocab.txt')
#
# print(voc_e)
# print(voc_f)
#
#
#
# #print(t_pairs)
#
# # def em_algorithm(voc_e, voc_f, s_pairs):
# #     test_probabs = initialise(voc_e, voc_f)
# #     s_total = {}
# #     number_iterations = 3
# #     for iteration in tqdm(range(number_iterations)):
# #         count = {}
# #         total = {}
# #
# #         for f_w in voc_f:
# #             total[f_w] = 0.0
# #             for e_w in voc_e:
# #                 count[(e_w, f_w)] = 0.0
# #         # print(f"These are the counts: {count}")
# #         # print(f"These are the totals: {total}")
# #         print('1/3 for loop done')
# #
# #         for pair in s_pairs:
# #             for e_w in pair[0]:  # // Compute normalization
# #                 s_total[e_w] = 0.0
# #                 for f_w in pair[1]:
# #                     s_total[e_w] += test_probabs[(e_w, f_w)]
# #             #print(f"These are the s_totals: {s_total}")
# #             # // Collect counts
# #             for e_w in pair[0]:
# #                 for f_w in pair[1]:
# #                     count[(e_w, f_w)] += test_probabs[(e_w, f_w)] / s_total[e_w]  # s_total = (1/len(pair[0]))
# #                     total[f_w] += test_probabs[(e_w, f_w)] / s_total[e_w]
# #         #print(f"These are the totals: {total}")
# #         print('2/3 for loop done')
# #         #print(t_probs)
# #         # print(count)
# #         # print(total)
# #         # print(s_total)
# #
# #         # // Estimate probabilities
# #         for f_w in voc_f:
# #             for e_w in voc_e:
# #                 test_probabs[(e_w, f_w)] = count[(e_w, f_w)] / total[f_w]
# #         print('3/3 for loop done')
# #         return test_probabs
# t_pairs = list(zip(e_sents, f_sents))
# print(t_pairs)

#____________
src_en =  read_corpus('src_en.txt')
tgt_fr =  read_corpus('tgt_fr.txt')
src_sents = tokenise_clean_sents(src_en)
print(e_sents)
tgt_sents = tokenise_clean_sents(tgt_fr)
print(tgt_sents)
pairs = list(zip(src_sents, tgt_sents))
print(pairs)

#____________

def em_no_null(t_pairs, source_vocab, target_vocab):  # Only works for first iteration
    #INITIALISE
    total = {}
    count_dict = {}
    translation_probabilities = {}

    #EXPECTATION
    if not translation_probabilities:
        for pair in t_pairs:
            for s_w in pair[0]:
                for t_w in pair[1]:
                    # when P(q|z) is uniformally initialized,
                    # the normalized count(q,z) is 1/(len(tgt_sent))
                    normalized_count = 1/len(pair[1])
                    # update count(q|z) and total(z)
                    if t_w in total:
                        total[t_w] += normalized_count
                    else:
                        total[t_w] = normalized_count
                    if (s_w, t_w) in count_dict:
                        count_dict[(s_w, t_w)] += normalized_count
                    else:
                        count_dict[(s_w, t_w)] = normalized_count

    #MAXIMIZATION
    for t_w in target_vocab:
        if t_w not in total:
            total[t_w] = normalized_count
        for s_w in source_vocab:
            if (s_w, t_w) not in count_dict:
                count_dict[(s_w, t_w)] = 0.0
    for (s_w, t_w), count in count_dict.items():
        conditional_prob = count / total[t_w]
        translation_probabilities[(s_w, t_w)] = conditional_prob


    return translation_probabilities



#__________________________________
def iterations(previous_probabilities, t_pairs, source_vocab, target_vocab):
    #INITIALISE
    total = {}
    s_total = {}
    count_dict = {}
    translation_probabilities = deepcopy(previous_probabilities)
    number_iterations = 4
    for iteration in tqdm(range(number_iterations)):
        #We don't initialise with 0.0 values, but keep that in mind and will update values later on
        for pair in t_pairs:
            for s_w in pair[0]:
                s_total[s_w] = 0.0
                for t_w in pair[1]:
                    if s_w in s_total:
                        s_total[s_w] += translation_probabilities[(s_w, t_w)]
                    else:
                        s_total[s_w] = translation_probabilities[(s_w, t_w)]
            for s_w in pair[0]:
                for t_w in pair[1]:
                    if (s_w, t_w) in count_dict:
                        count_dict[(s_w, t_w)] += translation_probabilities[(s_w, t_w)]/s_total[s_w]
                    else:
                        count_dict[(s_w, t_w)] = translation_probabilities[(s_w, t_w)]/s_total[s_w]
                    if t_w in total:
                        total[t_w] += translation_probabilities[(s_w, t_w)]/s_total[s_w]
                    else:
                        total[t_w] = translation_probabilities[(s_w, t_w)]/s_total[s_w]

        # MAXIMIZATION
        for t_w in target_vocab:
            for s_w in source_vocab:
                if (s_w, t_w) in count_dict:
                    translation_probabilities[(s_w, t_w)] = count_dict[(s_w, t_w)] / total[t_w]
                else:  # We update now the counts
                    #print(s_w, t_w)
                    count_dict[(s_w, t_w)] = translation_probabilities[(s_w, t_w)]/s_total[s_w]
                    translation_probabilities[(s_w, t_w)] = count_dict[(s_w, t_w)]/ total[t_w]
        print("End of loop.")
    return translation_probabilities




#_____________________________________________________-

def em(t_pairs, source_vocab, target_vocab):
    translation_probabilities = {}
    number_iterations = 4
    for iteration in tqdm(range(number_iterations)):

        #INITIALISE
        print("// Initialize")
        total = {}
        count_dict = {}
        conditional_prob = {}

        #EXPECTATION
        print("//E-Step")
        for t_w in target_vocab:
            total[t_w] = 0.0
            for s_w in source_vocab:
                count_dict[(s_w, t_w)] = 0.0
        print('1/3 for loop done')

        if not conditional_prob:
            print("Calculate conditional probabilities.")
            for pair in t_pairs:
                for s_w in pair[0]:
                    for t_w in pair[1]:
                        # when P(q|z) is initialized with uniform distribution,
                        # the normalized count(q,z) is 1/(length of target sent)
                        count_normalized = 1/len(pair[1])
                        # update count(q|z) and total(z)
                        total[t_w] += count_normalized
                        count_dict[(s_w, t_w)] += count_normalized
        # print(total)
        # print(count_dict)
        print('2/3 for loop done')

        #MAXIMIZATION
        print("//M-Step")
        for (s_w, t_w), count in count_dict.items():
            conditional_prob = count / total[t_w]
            translation_probabilities[(s_w, t_w)] = conditional_prob
    print('3/3 for loop done')
    return translation_probabilities

#_________________________________________________

def get_words_translations(translation_probabilities, parallel_corpus:list[tuple]):
    for src_sent, tgt_sent in parallel_corpus:
        print(src_sent, tgt_sent)  # Get translation probabilities for every sentence pair
        for s_w in src_sent:
            s_w_possible_translations = []
            for t_w in tgt_sent:
                s_w_index = src_sent.index(s_w)
                t_w_index = tgt_sent.index(t_w)
                tuple = (s_w, t_w), translation_probabilities[s_w, t_w], s_w_index, t_w_index  # Fetch the probabilities that are relevant for our word
                s_w_possible_translations.append(tuple)
            # Sort by highest probability
            sorted_probabilities = sorted(s_w_possible_translations, key=operator.itemgetter(1), reverse=True)
            # Get the one with the highest probability
            word_translation_n_alignment = sorted_probabilities[0]
            print(word_translation_n_alignment)  # The first index shown belongs to tgt_word, second index belongs to src_word
            #Need to tidy


# def get_index(pairs):  # Too nested
#     indexed_pairs = []
#     for pair in pairs:
#         s_sent = []
#         t_sent = []
#         for s_w in pair[0]:
#             word = s_w, pair[0].index(s_w)
#             s_sent.append(word)
#         print(s_sent)
#         for t_w in pair[1]:
#             word = t_w, pair[1].index(t_w)
#             t_sent.append(word)
#         pair = s_sent, t_sent
#         indexed_pairs.append(pair)
#     print(indexed_pairs)
#     return indexed_pairs



#_________________________________________________
# print(------------------THIS WORKS--------------------------)
# test_probabs = initialise(voc_e, voc_f)
# initial_prob = 1 / len(voc_f)
# #print(test_probabs)
# s_total = {}
# number_iterations = 3
# # for i, word in enumerate(tqdm(brown.words())):
# for iteration in tqdm(range(number_iterations)):
#     count = {}
#     total = {}
#
#     for f_w in voc_f:
#         total[f_w] = 0.0
#         for e_w in voc_e:
#             count[(e_w, f_w)] = 0.0
#     # print(f"These are the counts: {count}")
#     # print(f"These are the totals: {total}")
#     print('1/3 for loop done')
#
#     for pair in t_pairs:
#         #for e_w in pair[0]:  # // Compute normalization
#             # s_total[e_w] = 0.0  # We don't need s_total bc 1/Satzlänge der Zielsprache
#             # for f_w in pair[1]:
#             #     s_total[e_w] += test_probabs[(e_w, f_w)]
#         #print(f"These are the s_totals: {s_total}")
#         # // Collect counts
#         for e_w in pair[0]:
#             for f_w in pair[1]:
#                 count[(e_w, f_w)] += test_probabs[(e_w, f_w)] / (1/len(pair[0]))
#                 total[f_w] += test_probabs[(e_w, f_w)] / (1/len(pair[0]))
#     #print(f"These are the totals: {total}")
#     print('2/3 for loop done')
#     #print(t_probs)
#     # print(count)
#     # print(total)
#     # print(s_total)
#
#     # // Estimate probabilities
#     for f_w in voc_f:
#         for e_w in voc_e:
#             test_probabs[(e_w, f_w)] = count[(e_w, f_w)] / total[f_w]
#     print('3/3 for loop done')
# print('________________________________________')

#print(t_probs)  # translation prob. t(e|f)
print("________________TINY TEST_______________")
src_en =  read_corpus('src_en.txt')
tgt_fr =  read_corpus('tgt_fr.txt')
src_sents = tokenise_clean_sents(src_en)
print(e_sents)
tgt_sents = tokenise_clean_sents(tgt_fr)
print(tgt_sents)
pairs = list(zip(src_sents, tgt_sents))
print(pairs)
src_voc = get_words(src_sents)
tgt_voc = get_words(tgt_sents)
#test_probs = em_no_null(pairs, src_voc, tgt_voc)
#print(test_probs)
#print(em(pairs, src_voc, tgt_voc))
previous_translation_probabilities = em_no_null(pairs, src_voc, tgt_voc)
print(previous_translation_probabilities)
final_translation_probabilities = iterations(previous_translation_probabilities, pairs, src_voc, tgt_voc)
print(final_translation_probabilities)
alignments = get_words_translations(final_translation_probabilities, pairs)
print(alignments)
#print(alignments_2(final_translation_probabilities))
#print(get_index(pairs))



finish = time.time()
print(f"The algorithm took {finish-start} seconds.")